import Test.Tasty
import Test.Tasty.Hedgehog
import Test.Tasty.HUnit
import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Types

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ testGroup "getDiceAt (unit)" getDiceTests
  , testGroup "setDiceAt (random)" setDiceTests
  ]

getDiceTests :: [TestTree]
getDiceTests =
  [ testCase "getDiceAt (0,1)" $ let
      d = mkDice
      n = Nothing
      c = Combination [[ d 1, d 2, n ]
                      ,[ d 3, d 4, d 5]
                      ,[ n, n, n ]]
      in getDiceAt c 0 1 @?= d 2
  ]

genDice = Gen.element $ Nothing : map mkDice [1..6]

genCoord = Gen.integral (Range.constant 0 2)

genComb = Combination <$> genRows
  where genRows = Gen.list (Range.singleton 3) genCols
        genCols = Gen.list (Range.singleton 3) genDice

prop_getSet :: Property
prop_getSet = property $ do
  (c,i,j,d) <- forAll $
    (,,,) <$> genComb
    <*> genCoord
    <*> genCoord
    <*> genDice
  getDiceAt (setDiceAt c i j d) i j === d

setDiceTests :: [TestTree]
setDiceTests =
  [ testProperty "get . set" prop_getSet ]
