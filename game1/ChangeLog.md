# Changelog for "Petals around the Rose"

## Unreleased changes

### 2020-10-08

- описание в README (без спойлеров)
- проект
- типы для кубиков
- типы для комбинации кубиков
- типы для состояния игры (WIP)

### 2020-10-09

- типы для состояния игры
- функции для игры
- монады, монада State и Random
- функция для броска кубиков
- вспомогательные функции для изменения полей (см. также: линзы)
