{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Lib

import GHC.Generics
import Options.Generic
import Network.Wai.Handler.Warp
import Network.Wai.Middleware.RequestLogger

data CliOpts w = CliOpts
  { port :: w ::: Int          <?> "Port on which server runs"
  , host :: w ::: Maybe String <?> "Hostname for server"
  , timeout :: w ::: Maybe Int <?> "Timeout of Slowloris attack"
  } deriving (Generic)

instance ParseRecord (CliOpts Wrapped)
deriving instance Show (CliOpts Unwrapped)

main :: IO ()
main = do
  opts <- unwrapRecord "Petals Around a Rose server"
  -- print (opts :: CliOpts Unwrapped)
  let settings = setPort (port opts) $
        maybe id (setHost . read) (host opts) $
        maybe id setTimeout (timeout opts) $
        defaultSettings
  app <- mkApp
  runSettings settings $ logStdoutDev app
