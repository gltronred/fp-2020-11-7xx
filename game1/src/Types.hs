-- | Модуль для типов в игре

module Types
  ( module Types.Combination
  , module Types.Game
  ) where

import Types.Combination
import Types.Game
