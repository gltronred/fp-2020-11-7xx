-- | Функции для игры

module Game where

import Types

import Control.Monad.Random

{---- Несколько ходов
applyMoves gs m = let
  s0 = seed gs
  (r0,s1) = randomStuff s0
  (r1,s2) = randomStuff s1
  ...

Получается монада State
-}

-- | Применение хода к состоянию игры
applyMove :: RandomGen g
          => GameState -- ^ старое состояние игры
          -> Move      -- ^ ход
          -> Rand g GameState -- ^ новое состояние игры
applyMove gs Random = do
  comb <- randomCombination
  applyMove gs $ Ask comb
applyMove gs (Ask c) = pure $ askCombination c gs
applyMove gs NoGuess = pure $ checkGuess Nothing gs
applyMove gs (Guess ans) = pure $ checkGuess (Just ans) gs

-- | Чистая функция для задания комбинации
askCombination :: Combination -- ^ Задаваемая комбинация
               -> GameState   -- ^ Старое состояние игры
               -> GameState   -- ^ Новое состояние игры
askCombination c gs = setAskedCombination c $ case askedCombination gs of
  Nothing -> gs
  Just old -> appendOldCombinations old gs

-- | Чистая функция для проверки комбинации
checkGuess :: Maybe Int -- ^ Предполагаемый ответ
           -> GameState -- ^ Старое состояние игры
           -> GameState -- ^ Новое состояние игры
checkGuess mans gs = case askedCombination gs of
  Nothing -> gs
  Just c -> appendOldCombinations c $ clearAskedCombination $ case mans of
    Nothing -> gs { corrects = 0 }
    Just ans
      | ans == secretRule c -> gs { corrects = corrects gs + 1 }
      | otherwise -> gs { corrects = 0 }

-- | Начальное состояние игры
initialState :: GameState
initialState = GameState
  { corrects = 0
  , askedCombination = Nothing
  , oldCombinations = []
  }
