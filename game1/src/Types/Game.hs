-- | Состояние игры и ходы

{-# LANGUAGE DeriveGeneric #-}

module Types.Game where

import Types.Combination

import Data.Aeson
import GHC.Generics

-- | Возможные ходы игрока
data Move
  = Random
    -- ^ показываем случайную комбинацию и ответ для неё
  | Ask Combination
    -- ^ игрок задаёт комбинацию для показа
  | Guess Int
    -- ^ принимаем отгадку к показанной комбинации игрока
  | NoGuess
    -- ^ игрок просит показать ответ к показанной комбинации
  deriving (Eq,Show,Read,Generic)

instance FromJSON Move
instance ToJSON Move

-- | Состояние игры
data GameState = GameState
  { corrects :: Int
    -- ^ количество правильных отгадок подряд
  , askedCombination :: Maybe Combination
    -- ^ текущая комбинация (пока без ответа)
  , oldCombinations :: [(Combination, Int)]
    -- ^ комбинации, которые уже выпадали
  -- , seed :: Int
  --   -- ^ зерно генератора случайных чисел
  ----- Вариант: хранить все возможные комбинации
  ----- в том порядке, в котором выдаём игроку
  -- , combinations :: [Combination]
  } deriving (Eq,Show,Read,Generic)

instance FromJSON GameState
instance ToJSON GameState

-- | Установить текущую комбинацию (без ответа)
setAskedCombination :: Combination -- ^ Текущая комбинация
                    -> GameState   -- ^ Старое состояние игры
                    -> GameState   -- ^ Новое состояние
setAskedCombination c gs = gs { askedCombination = Just c }

-- | Очистить текущую комбинацию
clearAskedCombination :: GameState -- ^ Старое состояние игры
                      -> GameState -- ^ Новое состояние игры
clearAskedCombination gs = gs { askedCombination = Nothing }

-- | Добавить к старым комбинациям
appendOldCombinations :: Combination -- ^ Добавляемая комбинация
                      -> GameState   -- ^ Старое состояние
                      -> GameState   -- ^ Новое состояние
appendOldCombinations c gs = gs { oldCombinations = (c, secretRule c) : oldCombinations gs }
