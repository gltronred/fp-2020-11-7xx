-- |

module Types.Rule where

import Control.Monad
import Data.Maybe

import Types.Combination

type Pos = Int

data BFormula
  = BConst Bool
  | ValueAt Pos Int
  | And BFormula BFormula
  | Or BFormula BFormula
  deriving (Eq,Show,Read)

data Formula
  = Const Int
  | Plus Formula Formula
  | Mult Formula Formula
  | Count Int
  | IfThenElse BFormula Formula Formula
  deriving (Eq,Show,Read)

allFormulae :: Int -> [Formula]
allFormulae 0 = do
  x <- [1..2]
  case x of
    1 -> Const <$> [0..25]
    2 -> Count <$> [1..6]
allFormulae d = do
  x <- [0..d]
  case x of
    0 -> Const <$> [0..25]
    _ | x == d -> Count <$> [1..6]
      | otherwise -> do
        op <- [Plus,Mult]
        op <$> allFormulae x <*> allFormulae (d-x)
    -- 5 -> IfThenElse
    --      <$> bForm (d-1)
    --      <*> allFormulae (d-1)
    --      <*> allFormulae (d-1)

bForm :: Int -> [BFormula]
bForm 0 = do
  x <- [1..2]
  case x of
    1 -> BConst <$> [False,True]
    2 -> ValueAt <$> [0..4] <*> [1..6]
bForm d = do
  x <- [1..4]
  case x of
    1 -> BConst <$> [False,True]
    2 -> ValueAt <$> [0..4] <*> [1..6]
    3 -> And <$> bForm (d-1) <*> bForm (d-1)
    4 -> Or <$> bForm (d-1) <*> bForm (d-1)

flatten :: Combination -> [Dice]
flatten (Combination mdices) = catMaybes $ concat mdices

contradict :: (Combination, Int) -> Formula -> Bool
contradict (comb,val) formula = val /= eval formula dices
  where dices = flatten comb

eval :: Formula -> [Dice] -> Int
eval (Const v) _ = v
eval (Plus f1 f2) ds = eval f1 ds + eval f2 ds
eval (Mult f1 f2) ds = eval f1 ds * eval f2 ds
eval (Count v) ds = let Just dice = mkDice v in foldr (\d n -> if d == dice then n+1 else n) 0 ds
eval (IfThenElse b f1 f2) ds = if evalB b ds then eval f1 ds else eval f2 ds

evalB :: BFormula -> [Dice] -> Bool
evalB (BConst v) _ = v
evalB (ValueAt pos val) ds = Just (ds !! pos) == mkDice val
evalB (And f1 f2) ds = evalB f1 ds && evalB f2 ds
evalB (Or f1 f2) ds = evalB f1 ds || evalB f2 ds
