-- | Модуль для комбинаций кубиков

{-# LANGUAGE DeriveGeneric #-}

module Types.Combination
  ( Dice
  , mkDice
  , randomDice
  , Combination(..)
  , randomCombination
  , mkCombination
  , getDiceAt
  , setDiceAt
  , secretRule
  ) where

import Control.Monad.Random
import Data.Aeson
import Data.Maybe
import GHC.Generics

-- | Кубик: числа от 1 до 6
data Dice = Dice Int
  deriving (Eq,Show,Read,Generic)
----- Вариант:
-- data Dice = D1 | D2 | D3 | D4 | D5 | D6

instance FromJSON Dice
instance ToJSON Dice

-- | "Умный" конструктор
--
-- Создаёт кубик, если аргумент от 1 до 6
mkDice :: Int -> Maybe Dice
mkDice x
  | 1 <= x && x <= 6 = Just (Dice x)
  | otherwise = Nothing

mkCombination :: [Int] -> Combination
mkCombination vs = foldr (\((x,y),v) c -> setDiceAt c x y (Just $ Dice v)) emptyCombination $ zip places vs
  where emptyCombination = Combination $ replicate 3 $ replicate 3 Nothing
        places = [ (0,y) | y <- [0..2] ] ++ [ (1,y) | y <- [0..1] ]

-- | Случайный кубик
randomDice :: MonadRandom m => m Dice
randomDice = Dice <$> getRandomR (1,6)

-- | Комбинация из пяти кубиков
-- в "стакане" из девяти мест (3 на 3)
--
-- +---+---+---+
-- |o  |   |   |
-- | o | o |   |
-- |  o|   |   |
-- +---+---+---+
-- |   |o o|o o|
-- |   | o |o o|
-- |   |o o|o o|
-- +---+---+---+
-- |   |o o|   |
-- |   |   |   |
-- |   |o o|   |
-- +---+---+---+
--
data Combination
  = Combination [[Maybe Dice]]
    -- список списков кубиков
    -- наружний список - строки
    -- внутренние - кубики в этих строках
  deriving (Eq,Show,Read,Generic)
-- другой вариант:
-- Combination (Map (Int,Int) Dice)
-- тип Map в Data.Map пакета containers
-- но нужно корректно обрабатывать случай,
-- когда по заданным координатам нет клетки

instance FromJSON Combination
instance ToJSON Combination

getDiceAt :: Combination -> Int -> Int -> Maybe Dice
getDiceAt (Combination rows) i j
  = (rows !! i) !! j

setDiceAt :: Combination
          -> Int
          -> Int
          -> Maybe Dice
          -> Combination
setDiceAt (Combination rows) i j new
  = Combination $
    setElem rows i $
    setElem (rows !! i) j new
  where setElem :: [a] -> Int -> a -> [a]
        setElem list i el
          = take i list ++ [el] ++ drop (i+1) list

-- | Секретное правило,
-- которое должен угадать игрок
secretRule :: Combination -> Int
secretRule (Combination cs) = secretRule' $ catMaybes $ concat cs
  where secretRule' xs = sum $ flip map xs $ \(Dice d) -> case d of
          3 -> 2
          5 -> 4
          _ -> 0

-- | Случайная комбинация кубиков
randomCombination :: MonadRandom m => m Combination
randomCombination = do
  c <- fmap Combination $
       replicateM 3 $
       replicateM 3 $
       fmap Just $ randomDice
  let uniquesAcc acc [] = acc
      uniquesAcc acc (x:xs)
        | x `elem` acc = uniquesAcc acc xs
        | otherwise = x : uniquesAcc (x:acc) xs
      uniques xs = uniquesAcc [] xs
  emptys <- map (\k -> (k`div`3, k`mod`3)) .
            take 4 .
            uniques <$>
            getRandomRs (0,8)
  let comb = foldl (\c (i,j) -> setDiceAt c i j Nothing) c emptys
  pure comb
