-- | Описание API

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api
  ( module Api
  , BaseUrl (..)
  , Scheme (..)
  , ClientM
  ) where

import Types
import Game

import Control.Concurrent.STM
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Random
import Control.Monad.Random
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant.API
import Servant.Client
import Servant

type GameMonad g = ReaderT (TMVar GameState) (RandT g Handler)

type GameApi = "field" :> Get '[JSON] GameState
  :<|> "field" :> "combinations" :> Get '[JSON] [(Combination, Int)]
  :<|> "field" :> ReqBody '[JSON] Move :> Post '[JSON] GameState

fieldGet :: RandomGen g => GameMonad g GameState
fieldGet = do
  var <- ask
  state <- liftIO $ atomically $ readTMVar var
  pure state

combinationsGet :: RandomGen g => GameMonad g [(Combination, Int)]
combinationsGet = oldCombinations <$> fieldGet

fieldPost :: RandomGen g => Move -> GameMonad g GameState
fieldPost move = do
  var <- ask
  comb <- liftIO randomCombination
  liftIO $ atomically $ do
    state <- takeTMVar var
    let state' = case move of
          Random -> askCombination comb state
          Ask c -> askCombination c state
          NoGuess -> checkGuess Nothing state
          Guess i -> checkGuess (Just i) state
    putTMVar var state'
    pure state'


gameServer :: TMVar GameState -> Server GameApi
gameServer var = hoistServer gameApi gameToHandler $
                 fieldGet :<|> combinationsGet :<|> fieldPost
  where gameToHandler :: GameMonad StdGen a -> Handler a
        gameToHandler act = do
          g <- liftIO getStdGen
          evalRandT (runReaderT act var) g

gameApi :: Proxy GameApi
gameApi = Proxy

getField :: ClientM GameState
getCombinations :: ClientM [(Combination, Int)]
postMove :: Move -> ClientM GameState
getField :<|> getCombinations :<|> postMove = client gameApi

runClient :: BaseUrl -> ClientM a -> IO (Either ClientError a)
runClient baseUrl actions = do
  mgr <- newManager defaultManagerSettings
  let env = mkClientEnv mgr baseUrl
  runClientM actions env
