# Petals around a Rose (Лепестки у розы)

...поищите историю как Билл Гейтс играл в эту игру...

## Правила игры:

- игра называется "Лепестки у розы"
- для игры нужно пять игральных костей
- кидаются кубики (или просто раскладываются)
- для любой комбинации ведущий говорит ответ
- игрокам нужно догадаться, как устроено правило, по которому ведущий считает ответ

Например, для комбинации
- 4 3 1 4 3 - ответ 4
- 4 4 5 5 1 - ответ 8
- 5 5 5 5 5 - ответ 20
- и т.д.

# Полезные инструменты и библиотеки

- Hoogle: поиск по типам и именам функций в hackage: https://hoogle.haskell.org/
- Hayoo: то же самое: http://holumbus.fh-wedel.de/

## Использованные библиотеки:

- [MonadRandom](https://hackage.haskell.org/package/MonadRandom) - случайные числа
- [containers](https://hackage.haskell.org/package/containers) - разные контейнеры (например, Map)
- [lens](https://hackage.haskell.org/package/lens) - линзы (ООП на функциональном языке) - https://artyom.me/lens-over-tea-1
- [aeson](https://hackage.haskell.org/package/aeson) - библиотека работы с JSON
- [stm](https://hackage.haskell.org/package/stm) - Software Transactional Memory
- [servant](https://hackage.haskell.org/package/servant) - способ описания API на уровне типов
- [optparse-applicative](http://hackage.haskell.org/package/optparse-applicative) - пакет для парсинга опций командной строки (на основе аппликативных функторов)
- [optparse-generic](http://hackage.haskell.org/package/optparse-generic) - пакет для парсинга опций командной строки
- [gloss](http://hackage.haskell.org/package/gloss) - графика на OpenGL
- [free](http://hackage.haskell.org/package/free) - свободные монады
- [polysemy](http://hackage.haskell.org/package/polysemy) - более современная вариация свободных монад
