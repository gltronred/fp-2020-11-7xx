-- |

module L4 where

import Data.Char

-- Дерево с произвольным
-- количеством детей у узла
data Tree a
  = Leaf a -- Лист
  | Node a [Tree a] -- Промежуточный
  --        ^- список поддеревьев
  deriving (Show)

data NoShow = NoShow

data Val
  = Const Int
  | Var Char

-- Show - для преобразования в строку
-- не для красивого вывода, а для печати значений
-- Read - для чтения из строки
-- лучше всего объявлять одновременно оба
-- лучше всего использовать deriving (Show,Read)
instance Show Val where
  show (Const x) = "Const " ++ show x
  show (Var c) = "Var " ++ show c
  -- showList xs = \s -> "Another impl"

-- Eq - для проверки на равенство
instance Eq a => Eq (Tree a) where
  Leaf x == Leaf y       = x == y
  Node x xs == Node y ys = x == y && xs == ys
  _ == _                 = False
  -- По умолчанию, неравенство определено так:
  -- a /= b = not $ a == b
  -- Можно написать свою, если есть более оптимальная реализация
  -- Можно, наоборот, определить всё через (/=)
  -- В документации на hackage.haskell.org

instance Eq Val where
  Const x == Const y = x == y
  Var x == Var y = x == y
  _ == _ = False

-- Для сравнения. Требует Eq Val
-- Если убрать инстанс Eq Val выше, не скомпилируется
instance Ord Val where
  compare (Const x) (Const y) = compare x y
  compare (Const _) (Var _) = LT
  compare (Var x) (Var y) = compare x y
  compare (Var _) (Const _) = GT

---- !!! не работает !!!
-- instance Eq (Tree Int) where
--   (==) a b = undefined

---- Вот так, если нужно разное поведение:
data OtherTree a = OtherTree (Tree a)

-- дальше можно определять Eq (OtherTree a)
-- с другим равенством

data Arith
  = Number Int      -- n
  | Sum [Arith]     -- a1+a2+...+an
  | Mult [Arith]    -- a1*a2*...*an
  | Power Arith Int -- a^n
  deriving (Show)

instance Eq Arith where
  Number a == Number b = a == b
  Sum as == Sum bs = as == bs
  Mult as == Mult bs = as == bs
  Power a n == Power b m = a == b && n == m
  _ == _ = False

data BinTree a
  = EmptyTree
  | OneTree (BinTree a) a
  | BinTree (BinTree a) a (BinTree a)
  deriving (Show)

instance Eq a => Eq (BinTree a) where
  (==) EmptyTree EmptyTree = True
  (==) (OneTree a s) (OneTree b t) = a==b && s==t
  (==) (BinTree a1 s a2) (BinTree b1 t b2)
    = a1 == b1 && s == t && a2 == b2
  (==) _ _ = False

data G b = G { unG :: Bool -> b }
instance (Eq b) => Eq (G b) where
  G f == G g = f False == g False && f True == g True

myFun1, myFun2, myFun3 :: G Int
myFun1 = G $ \x -> if x then 1 else 0
myFun2 = G $ \x -> if x then 3 else 0
myFun3 = G $ \x -> 1 - unG myFun1 (not x)

-------------------------------------------------------
-- Функторы

-- class Functor f where
--   fmap :: (a -> b) -> f a -> f b

instance Functor Tree where
  fmap f (Leaf a) = Leaf $ f a
  fmap f (Node a subtrees)
    = Node (f a) $ fmap (fmap f) subtrees

data F r a = F { unF :: r -> a }
instance Functor (F r) where
  fmap f (F g) = F $ f . g     -- F $ fmap f g
  -- f :: a -> b
  -- g :: r -> a
  -- fmap f g :: r -> b

instance Functor BinTree where
  fmap f EmptyTree = EmptyTree
  fmap f (OneTree t r)
    = OneTree (fmap f t) (f r)
  fmap f (BinTree t1 r t2)
    = BinTree (fmap f t1) (f r) (fmap f t2)

---------------------------------------------------
-- Аппликативные функторы

-- class Functor f => Applicative f where
--   pure :: a -> f a
--   (<*>) :: f (a -> b) -> f a -> f b

data Parser a = Parser
  { runParser :: String -> [(String,a)] }
--                 ^           ^    ^
--                вход    остаток  результат

instance Functor Parser where
  fmap f (Parser parser)
    = Parser $ fmap (fmap (fmap f)) parser

instance Applicative Parser where
  pure a = Parser $ \s -> [(s,a)]
  Parser pf <*> Parser px = Parser $ \s -> let
    fs = pf s
    applyF (restF, f) = map (\(rest,x) -> (rest, f x)) $
                        px restF
    in concatMap applyF fs

anyChar :: Parser Char
anyChar = Parser $ \s -> case s of
  [] -> []
  (c:cs) -> [(cs, c)]

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser $ \s -> case s of
  [] -> []
  (c:cs)
    | p c -> [(cs, c)]
    | otherwise -> []

-- (<$>) = fmap

digit :: Parser Int
digit = toDigit <$> satisfy (\c -> c `elem` "0123456789")
  where toDigit c = ord c - ord '0'

pairDigit :: Parser (Int,Int)
pairDigit = (,) <$> digit <*> digit

-- identifier = (:) <$> anyChar <*> many anyChar
