# Функциональное программирование

Зиятдинов Мансур Тагирович
mz@lambdasoft.ru

Ссылка на репозиторий будет в MS Teams - код в расписании

## Haskell

https://haskell.org

- чисто функциональный
- статическая система типов
- ленивые вычисления

## Stack

Система сборки

http://docs.haskellstack.org/en/stable/install_and_upgrade/

- Создание проекта:`stack new p1`
- Сборка: `stack build`
- REPL: `stack repl`

## Компилятор и интерпретатор

GHC - Glasgow Haskell Compiler
GHCi - ... interpreter

Команды интерпретатора:
- выход `:q` или `:quit`
- перезагрузка файла `:r` или `:reload`

## Документация

http://hackage.haskell.org/ - место для библиотек

http://hackage.haskell.org/package/base - стандартная библиотека

http://hackage.haskell.org/package/base-4.12.0.0/docs/Prelude.html - стандартные функции

https://www.haskell.org/documentation/ - документация

https://stepik.org/course/75 - курс Haskell на русском

Книги (есть на русском):
- Learn You a Haskell for Great Good! Изучай    Haskell во имя добра!
