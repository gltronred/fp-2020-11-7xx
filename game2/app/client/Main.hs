-- |

{-# LANGUAGE DeriveFunctor #-}

module Main where

import Lib
import Types

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Free
import Data.Functor.Identity
import Data.List

prettyCard :: Card -> String
prettyCard (Card (Value v) s) =
  [ allValuesString !! (v-2)
  , allSuitsString !! fromEnum s
  , ' '
  ]

printHand :: [Card] -> IO ()
printHand cards = do
  forM_ cards $ \c -> do
    putStr $ prettyCard c
  putStrLn ""

-- ... reverse ...
-- [R3D, R5H, L5C, L5D, R6D]
-- ... groupBy ...
-- [[R3D], [R5H], [L5C, L5D], [R6D]]
-- ... ??? ...
-- [[R3D], [R5H, L5C, L5D], [R6D]]
-- ... ??? ...
-- [[J3D,N,N], [J5H,J5C,J5D], [R6D,N,N]]
-- ... transpose ...
tableToRows :: Table -> [[Maybe Card]]
tableToRows = transpose .
              padMax .
              groupBy eq .
              reverse
  where eq (Left  _) (Left  _) = True
        eq (Left  _) (Right _) = False
        eq (Right _) (Left  _) = True
        eq (Right _) (Right _) = False

padMax :: [[Either Card Card]] -> [[Maybe Card]]
padMax cols = let
  maxLen = maximum $ map length cols
  e2m (Left  c) = Just c
  e2m (Right c) = Just c
  in map (padTo maxLen . map e2m) cols

padTo :: Int -> [Maybe a] -> [Maybe a]
padTo n list = let
  m = length list
  in list ++ replicate (n-m) Nothing

printTable :: Table -> IO ()
printTable table = do
  let rows = tableToRows table
  forM_ rows $ \row -> do
    forM_ row $ \mcard -> case mcard of
      Nothing   -> putStr "   "
      Just card -> putStr $ prettyCard card
    putStrLn ""

---

data CommandF next
  = AskState (GameState -> next)
  | AskTable (Table -> next)
  | AskUserInput (Maybe Card -> next)
  | PrintAll [Card] Table next
  | MakeMove Move next
  | Quit next
  deriving (Functor)

type Program = Free CommandF

askState :: Program GameState
askState = liftF $ AskState id

askTable :: Program Table
askTable = liftF $ AskTable id

askUserInput :: Program (Maybe Card)
askUserInput = liftF $ AskUserInput id

printAll :: [Card] -> Table -> Program ()
printAll cards table = liftF $ PrintAll cards table ()

makeMove :: Move -> Program ()
makeMove move = liftF $ MakeMove move ()

quit :: Program ()
quit = liftF $ Quit ()

---

interpretAskUser :: ClientM (Maybe Card)
interpretAskUser = do
  input <- liftIO getLine
  case input of
    "quit" -> pure Nothing
    _ -> case parseCard input of
      Just c -> pure $ Just c
      _ -> interpretAskUser

interpretProgram :: Program () -> ClientM ()
interpretProgram (FreeT prog) = case runIdentity prog of
  Pure a -> pure a
  Free cmd -> case cmd of
    AskState next -> do
      st <- getState
      interpretProgram $ next st
    AskTable next -> do
      t <- getTable
      interpretProgram $ next t
    AskUserInput next -> do
      mcard <- interpretAskUser
      interpretProgram $ next mcard
    PrintAll myHand table next -> do
      liftIO $ printHand myHand
      liftIO $ printTable table
      interpretProgram next
    MakeMove move next -> do
      postMove move
      interpretProgram next
    Quit _next -> pure ()

---

game :: Program ()
game = forever $ do
  st <- askState
  let myHand = hand $ players st !! move st
  t <- askTable
  printAll myHand t
  cmd <- askUserInput
  case cmd of
    Nothing -> quit
    Just move -> makeMove move

-- game :: ClientM ()
-- game = do
--   st <- getState
--   let myHand = hand $ players st !! move st
--   t <- getTable
--   liftIO $ printHand myHand
--   liftIO $ printTable t
--   cmd <- liftIO getLine
--   case cmd of
--     "quit" -> pure ()
--     _ -> case parseCard cmd of
--       Just c -> postMove c >> game
--       _ -> game

main :: IO ()
main = do
  let baseUrl = BaseUrl Http "localhost" 8080 ""
  res <- runClient baseUrl $ interpretProgram game
  case res of
    Left err -> print err
    Right r -> pure ()
