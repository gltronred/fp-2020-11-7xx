-- | Функции для игры

module Game where

import Types

import Control.Monad.Random
import Data.Either
import Data.List

checkRule :: Rule -> Table -> Card -> Table
checkRule rule table newCard = let
  goodCards = rights table
  in if checkRuleGood rule (newCard : goodCards)
     then Right newCard : table
     else Left  newCard : table

checkRuleGood :: Rule -> [Card] -> Bool
checkRuleGood rule cards = case rule of
  And rs -> all (\r -> checkRuleGood r cards) rs
  Or  rs -> any (\r -> checkRuleGood r cards) rs
  Not r  -> not $ checkRuleGood r cards
  Pred p -> computePred p cards

computePred :: Predicate -> [Card] -> Bool
computePred pred cards = case pred of
  ValueGt v1 v2 ->
    compute value v1 cards > compute value v2 cards
  ValueEq v1 v2 ->
    compute value v1 cards == compute value v2 cards
  SuitEq v1 v2 ->
    compute suit v1 cards == compute suit v2 cards

compute :: (Card -> a) -> Var a -> [Card] -> Maybe a
compute _f (Const c) _cards = Just c
compute f (Place i) cards = if i < length cards
                            then Just $ f $ cards !! i
                            else Nothing

-- Здесь нет случайного выбора
-- Если бы был, то функция была бы
-- :: RandomGen g => GameState -> Move -> Rand g GameState
-- (здесь RandomGen и Rand из библиотеки MonadRandom)
--
-- Но в любом случае вам нужна чистая функция,
-- которая применяет ход
addCardToTable :: GameState -> Move -> Either String GameState
addCardToTable gs card = do
  let m = move gs
      currentHand = players gs !! m
  newHand <- removeCard card currentHand
  pure $ gs
    { table = checkRule (rule gs) (table gs) card
    , players = take m (players gs) ++
                [newHand] ++
                drop (m+1) (players gs)
    , move = (move gs + 1) `mod` (length $ players gs)
    }

removeCard :: Card -> Player -> Either String Player
removeCard card player = if card `elem` hand player
                         then Right $ player { hand = delete card $ hand player }
                         else Left "No such card"

initialState :: (MonadRandom m, MonadFail m)
             => Int
             -> Rule
             -> m GameState
initialState count theRule = do
  firstCard : shuffledDecks <- shuffle $ fullDeck ++ fullDeck
  pure $ GameState
    { table = [Right firstCard]
    , players = take count $
                map (\h -> Player h 0) $
                splitBy 12 shuffledDecks
    , move = 0
    , rule = theRule
    }

fullDeck :: [Card]
fullDeck = do
  value <- map Value [2..14]
  suit <- [Hearts .. Clubs]
  pure $ Card value suit
-- map Value [2..14] >>= \value ->
--   [Hearts .. Clubs] >>= \suit ->
--      [Card value suit]

shuffle :: MonadRandom m => [a] -> m [a]
shuffle [] = pure []
shuffle xs = do
  i <- getRandomR (0,length xs-1)
  let el = xs !! i
      rest = take i xs ++ drop (i+1) xs
  (el :) <$> shuffle rest

splitBy :: Int -> [a] -> [[a]]
splitBy k xs
  | length xs == 0 = []
  | length xs <= k = [xs]
  | otherwise = take k xs : splitBy k (drop k xs)
