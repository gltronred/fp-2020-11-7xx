-- | Карты

{-# LANGUAGE DeriveGeneric #-}

module Types.Card where

import Data.Aeson(FromJSON,ToJSON)
import Data.List (findIndex)
import GHC.Generics

-- Масть
data Suit
  = Hearts
  | Diamonds
  | Spades
  | Clubs
  deriving (Eq,Enum,Show,Read,Generic)

instance FromJSON Suit
instance ToJSON Suit

-- Значение карты, 2..14
data Value = Value Int
  deriving (Eq,Ord,Show,Read,Generic)

instance FromJSON Value
instance ToJSON Value

-- Карта
data Card = Card
  { value :: Value
  , suit :: Suit
  } deriving (Eq,Show,Read,Generic)

instance FromJSON Card
instance ToJSON Card

allValuesString, allSuitsString :: String
allValuesString = "23456789TJQKA"
allSuitsString = "HDSC"

parseCard :: String -> Maybe Card
parseCard [v,s]
  = mkCard
  <$> findIndex (==v) allValuesString
  <*> findIndex (==s) allSuitsString
  where mkCard vi si = Card (Value $ vi + 2) (toEnum si)
parseCard _ = Nothing
