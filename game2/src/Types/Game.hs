-- | Типы для самой игры

{-# LANGUAGE DeriveGeneric #-}

module Types.Game where

import Types.Card
import Types.Rules

import Data.Aeson
import GHC.Generics

-- Последовательность карт,
-- либо неправильно (Left),
-- либо правильно (Right) выложенных
--
-- Последняя выложенная карта - в голове списка
type Table = [Either Card Card]

data Player = Player
  { hand :: [Card]
  , points :: Int
  } deriving (Eq,Show,Read,Generic)

instance FromJSON Player
instance ToJSON Player

type Move = Card

data GameState = GameState
  { table :: Table
  , players :: [Player]
  , move :: Int
  , rule :: Rule
  } deriving (Eq,Show,Read,Generic)

instance FromJSON GameState
instance ToJSON GameState
