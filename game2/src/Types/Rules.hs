-- | Правила

{-# LANGUAGE DeriveGeneric #-}

module Types.Rules where

import Types.Card

import Data.Aeson(FromJSON,ToJSON)
import GHC.Generics

data Var a
  = Place Int
    -- 0 - последняя, 1 - предпоследняя и т.д.
  | Const a
    -- значения
  deriving (Eq,Show,Read,Generic)

instance FromJSON a => FromJSON (Var a)
instance ToJSON a => ToJSON (Var a)

data Predicate
  = ValueGt (Var Value) (Var Value)
  | ValueEq (Var Value) (Var Value)
  | SuitEq (Var Suit) (Var Suit)
  deriving (Eq,Show,Read,Generic)

instance FromJSON Predicate
instance ToJSON Predicate

data Rule
  = And [Rule]
  | Or [Rule]
  | Not Rule
  | Pred Predicate
  deriving (Eq,Show,Read,Generic)

instance FromJSON Rule
instance ToJSON Rule

exampleRule :: Rule
exampleRule = Or
  [ Pred $ Place 0 `ValueGt` Place 1
  , And
    [ Or
      [ Pred $ Place 1 `ValueEq` Const (Value 11)
      , Pred $ Place 1 `ValueEq` Const (Value 12)
      , Pred $ Place 1 `ValueEq` Const (Value 13)
      , Pred $ Place 1 `ValueEq` Const (Value 14)
      ]
    , Or
      [ Pred $ Place 0 `ValueEq` Const (Value 2)
      , Pred $ Place 0 `ValueEq` Const (Value 3)
      , Pred $ Place 0 `ValueEq` Const (Value 4)
      , Pred $ Place 0 `ValueEq` Const (Value 5)
      , Pred $ Place 0 `ValueEq` Const (Value 6)
      , Pred $ Place 0 `ValueEq` Const (Value 7)
      , Pred $ Place 0 `ValueEq` Const (Value 8)
      , Pred $ Place 0 `ValueEq` Const (Value 9)
      , Pred $ Place 0 `ValueEq` Const (Value 10)
      ]
    ]
  ]
