-- | Заготовка типа для API

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api
  ( module Api
  , BaseUrl (..)
  , Scheme (..)
  , ClientM
  ) where

import Types
import Game

import Control.Concurrent.STM
import Control.Monad.IO.Class
import Control.Monad.Random
import Control.Monad.Reader
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant
import Servant.API
import Servant.Client

type GameAPI = "state" :> Get '[JSON] GameState
  :<|> "state" :> "table" :> Get '[JSON] Table
  :<|> "move" :> ReqBody '[JSON] Move :> Post '[JSON] GameState

type AppM = ReaderT (TMVar GameState) Handler


gameServer :: ServerT GameAPI AppM
gameServer = stateGet :<|> stateTableGet :<|> movePost

stateGet :: AppM GameState
stateGet = do
  sv <- ask
  s <- liftIO $ atomically $ readTMVar sv
  pure s

stateTableGet :: AppM Table
stateTableGet = table <$> stateGet

movePost :: Move -> AppM GameState
movePost m = do
  sv <- ask
  s' <- liftIO $ atomically $ do
    s <- takeTMVar sv
    case addCardToTable s m of
      Left e -> pure $ Left e
      Right s' -> do
        putTMVar sv s'
        pure $ Right s'
  case s' of
    Left _e -> throwError err401
    Right s' -> pure s'


gameApi :: Proxy GameAPI
gameApi = Proxy


mkApp :: IO Application
mkApp = do
  g <- newStdGen
  s <- evalRandT (initialState 2 exampleRule) g
  sv <- newTMVarIO s
  pure $
    serve gameApi $
    hoistServer gameApi (flip runReaderT sv) gameServer

-- do-нотация раскрывается в цепочку >>=
--
-- newStdGen >>= (\g -> evalRandT...g >>= (\s -> newTMVarIO s >>= (\sv -> pure ...)))

getState :: ClientM GameState
getTable :: ClientM Table
postMove :: Move -> ClientM GameState
getState :<|> getTable :<|> postMove = client gameApi

runClient :: BaseUrl -> ClientM a -> IO (Either ClientError a)
runClient baseUrl actions = do
  mgr <- newManager defaultManagerSettings
  runClientM actions $ mkClientEnv mgr baseUrl
