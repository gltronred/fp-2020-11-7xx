
import Test.Tasty
import Test.Tasty.HUnit hiding (assert)
import Test.Tasty.Hedgehog

import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Types
import Game

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ testGroup "HUnit tests" hunits
  , testGroup "Hedgehog tests" hedgehogs
  ]

hunits :: [TestTree]
hunits =
  [ testCase "Remove 2H from [2H,3S,4C]" $ let
      card = Card (Value 2) Hearts
      player = Player [ card
                      , Card (Value 3) Spades
                      , Card (Value 4) Clubs
                      ] 0
      player' = Player [ Card (Value 3) Spades
                       , Card (Value 4) Clubs
                       ] 0
      in removeCard card player @?= Right player'
  , testCase "splitBy 2 [1,2,3] == [[1,2],[3]]" $
    splitBy 2 [1,2,3] @?= [[1,2],[3]]
  ]

hedgehogs :: [TestTree]
hedgehogs =
  [ testProperty "All chunks in splitBy k has length <= k" $
    property $ do
      xs <- forAll $
            Gen.list (Range.linear 0 10000) $
            Gen.int (Range.linear 0 1000)
      k <- forAll $ Gen.filter (>0) $ Gen.int (Range.linear 0 100)
      let chunks = splitBy k xs
      assert $ all ((<=k) . length) chunks
  , testProperty "Last chunk has length <=k" $
    property $ do
      xs <- forAll $
            Gen.list (Range.linear 1 10000) $
            Gen.int (Range.linear 0 1000)
      k <- forAll $ Gen.int (Range.linear 1 100)
      let chunks = splitBy k xs
      annotateShow chunks
      diff (head chunks) ((<=) . length) k
  ]
