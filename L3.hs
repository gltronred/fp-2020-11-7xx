-- |

module L3 where

-- data [a] = [] | a : [a]

-- Стандартная функция length
-- length
len :: [a] -> Int
len = foldr (\_ acc -> acc+1) 0

-- Объединение двух списков
-- Стандартная функция (++)
-- append [1,2] [3,4,5] == [1,2,3,4,5]
append :: [a] -> [a] -> [a]
append xs ys = foldr (:) ys xs

-- Список без последнего элемента
-- Стандартная функция init
-- init1 [1,2,3] == [1,2]
init1 :: [a] -> [a]
init1 [] = [] -- стандартная init выкидывает ошибку
-- init1 (x:[]) = []
init1 [_x] = []
init1 (x:xs) = x : init1 xs

-- Группировка одинаковых элементов
-- Стандартная Data.List.group
-- group [1,1,2,3,3,3] == [[1,1],[2],[3,3,3]]
groupInt :: [Int] -> [[Int]]
groupInt [] = []
groupInt [x] = [[x]]
groupInt (x:y:rest)
  | x /= y    = [x] : groupInt (y:rest)
  | otherwise = let
      gs : gss = groupInt (y:rest)
      in (x:gs) : gss

-- Последний элемент списка
-- Стандартная функция last :: [a] -> a
-- last1 [1,2,3] == Just 3
last1 :: [a] -> Maybe a
last1 [] = Nothing
last1 [x] = Just x
last1 (_:xs) = last1 xs

-- Проверка, является ли префиксом
-- Data.List.isPrefixOf
-- isPrefixOf "Hello" "Hello, world!" == True
isPrefixOf :: Eq a => [a] -> [a] -> Bool
isPrefixOf [] _ys = True
isPrefixOf (_:_) [] = False
isPrefixOf (x:xs) (y:ys) = x==y && isPrefixOf xs ys

-- Отбрасывание префикса
-- Data.List.stripPrefix
-- stripPrefix "foo" "foobar" == Just "bar"
-- stripPrefix "baz" "foobar" == Nothing
stripPrefix :: Eq a => [a] -> [a] -> Maybe [a]
stripPrefix = undefined

-- Data.List.intersperse
-- intersperse 1 [2,3,4,5] == [2,1,3,1,4,1,5]
intersperse :: a -> [a] -> [a]
intersperse = undefined

-- Все возможные подпоследовательности
-- Data.List.subsequences
-- subsequences [1,2,3] == [[],[2],[3],[2,3],[1],[1,2],[1,3],[1,2,3]]
subsequences :: [a] -> [[a]]
subsequences [] = [[]]
subsequences (x:xs) = let
  tailSubs = subsequences xs
  -- (\ys -> x:ys)
  -- то же, что и
  -- (x:)
  in tailSubs ++ map (x:) tailSubs

-- Транспонирование
-- Data.List.transpose
-- transpose [[1,2,3],[4,5,6]] == [[1,4],[2,5],[3,6]]
-- Будем гарантировать только
-- при одинаковых длинах списков
transpose :: [[a]] -> [[a]]
transpose [] = []
transpose (list : lists) = let
  transposed = transpose lists
  -- [[1,2],[3,4],[5,6]] --> [[1,3,5],[2,4,6]]
  -- [[3,4],[5,6]] --> [[3,5],[4,6]]
  addToLists :: [a] -> [[a]] -> [[a]]
  addToLists [] ls = ls
  addToLists (x:xs) [] = [x] : addToLists xs []
  addToLists (x:xs) (l:ls) = (x:l) : addToLists xs ls
  in addToLists list transposed

-- Стандартная map
myMap :: (a -> b) -> [a] -> [b]
myMap _f [] = []
myMap f (x:xs) = f x : myMap f xs

-- Стандартная filter
myFilter :: (a -> Bool) -> [a] -> [a]
myFilter _p [] = []
myFilter p (x:xs)
  | p x = x : myFilter p xs
  | otherwise = myFilter p xs

-- Стандартная foldl
myFoldl :: (b -> a -> b) -> b -> [a] -> b
myFoldl _f z []     = z
myFoldl  f z (x:xs) = myFoldl f (z `f` x) xs

-- Стандартная foldr
myFoldr :: (a -> b -> b) -> b -> [a] -> b
myFoldr _f z []     = z
myFoldr  f z (x:xs) = x `f` myFoldr f z xs

-- Построим функцию map, используя функцию foldr
--
-- Что можно узнать о типах?
-- 1. b = [y]
-- 2. (a -> b -> b) = (a -> [y] -> [y])
-- 3. Наверное? [a] = [x]
-- 4. a = x
-- 5. (a -> b -> b) -> (x -> [y] -> [y])
--
-- Какие у нас есть значения и какие у них типы?
-- 6. g :: x -> y
-- 7. xs :: [x]    - поэтому третий аргумент foldr такой
-- 8. для второго аргумента нужно выражение типа :: [y] - это []
-- 9. для первого аргумента нужно выражение типа :: x -> [y] -> [y]
-- 10.1. создадим лямбда-функцию с аргументами x :: x и ys :: [y]
-- 10.2. теперь есть g :: x -> y, x :: x, ys :: [y]
-- 10.3. нужно построить [y]
-- 10.4. варианты [] :: [y] и ys :: [y] неинтересные
-- 10.5. остаётся вариант y : ys, где y - какое-то выражение :: y
-- 10.6. нужный тип у выражения g x :: y
-- 11. Можно убрать xs - он в обеих частях
mapViaFoldr :: (x -> y) -> [x] -> [y]
mapViaFoldr g = foldr (\x ys -> g x : ys) []
