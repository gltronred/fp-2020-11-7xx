-- |

module L2 where

-- :t (:type) - просмотр типа
-- :t +d      - вместо наиболее
--              общего типа
--              подставить тип по умолчанию

-- :i (:info) - информация о идентификаторе

-- data Bool = False | True

data Student1 = Present
              | Online
              | Absent

mark1 :: Student1 -> Int
mark1 Present = 100
mark1 Online = 80
mark1 Absent = 0

data Body = Body Int Double

volume :: Body -> Double
volume (Body 0 _) = 0.01
volume (Body height weight)
  = fromIntegral height * weight

data Student2 = Present2 Int
              | Online2 Double
              | Absent2

mark2 :: Student2 -> Int
mark2 student = case student of
  Present2 100 -> 100
  Present2 x
    | 50 < x -> 99
    | otherwise -> 90
  Online2 d
    | d > 100.0 -> 80
    | d > 50.0  -> 70
    | otherwise -> 60
  Absent2 -> 0

---- Типы с аргументами
-- data Maybe a = Nothing | Just a

---- Рекурсивные типы
data List a
  = Nil
  | Cons a (List a)
  deriving (Show) -- пока магия для вывода

len :: List a -> Int
len Nil = 0
len (Cons _ tl) = len tl + 1

-- append :: List a -> (List a -> List a)
append :: List a -> List a -> List a
append Nil list2 = list2
append (Cons hd tl) list2 = Cons hd $ append tl list2

-- есть в Data.Maybe
fromMaybe :: a -> Maybe a -> a
fromMaybe def Nothing = def
fromMaybe _ (Just val) = val

data Point1 = Point1 Double Double deriving (Show)
pt1x (Point1 x _) = x
pt1y (Point1 _ y) = y

data Point = Point
  { pointX :: Double
  , pointY :: Double
  } deriving (Show)

-- let pt = Point 1 2
-- let pt' = Point { pointY = 2, pointX = 3 }
-- pt { pointY = 3.5 }

data Tree a
  = Leaf { el :: a }
  | Branch { left :: Tree a
           , el :: a
           , right :: Tree a
           }
  deriving (Show)

safeLeft :: Tree a -> Maybe (Tree a)
safeLeft (Leaf _) = Nothing
safeLeft (Branch l _ _) = Just l

safeRight :: Tree a -> Maybe (Tree a)
safeRight (Leaf _) = Nothing
safeRight (Branch _ _ r) = Just r

depth :: Tree a -> Int
depth (Leaf _) = 1
depth (Branch l _ r) = 1 + depth l `max` depth r
--                                   ^--- обычная max как инфиксная
-- = 1 + max (depth l) (depth r)

sumTree :: Tree Int -> Int
sumTree (Leaf x) = x
sumTree (Branch l x r) = sumTree l + x + sumTree r

tree1 :: Tree Int
tree1 = Branch
  (Branch (Leaf 1) 2 (Leaf 3))
  4
  (Leaf 5)
{-
      4
     / \
    2   5
   / \
  1   3
-}

leaves :: Tree a -> Int
leaves (Leaf x) = 1
leaves (Branch l x r) = leaves l + leaves r

data Arith
  = Const Int        -- n       - число
  | Plus Arith Arith -- e1 + e2 - сумма
  | Mult Arith Arith -- e1 * e2 - произведение
  | Exp  Arith Int   -- e1 ^ n  - возведение в степень
  deriving (Show)

ar1 = Mult (Const 3) (Plus (Const 1) (Const 2))
ar2 = (Const 2 `Plus` Const 2) `Mult` Const 2
ar3 = Plus (Const 0) (Const 3)
ar4 = Exp (Const 3) 4
ar5 = Exp (Const 1 `Plus` Const 2) 2

evalArith :: Arith -> Int
evalArith ex = case ex of
  Const x -> x
  Plus e1 e2 -> evalArith e1 + evalArith e2
  Mult e1 e2 -> evalArith e1 * evalArith e2
  Exp  e1 e2 -> evalArith e1 ^ e2

{- Функции приведения типов
fromIntegral - из "целых"
realToFrac
-- "вещественные" в "целые"
truncate
round
ceiling
floor
-}

-- simplArith ar1 ->
-- Just $ Plus (Mult (Const 3) (Const 1))
--             (Mult (Const 3) (Const 2))
--
-- simplArith ar2 ->
-- Just $ Plus (Mult (Const 2) (Const 2))
--             (Mult (Const 2) (Const 2))
--
-- simplArith ar3 ->
-- Nothing
--
-- simplArith ar5 ->
-- Just $ Mult (Const 1 `Plus` Const 2)
--             (Const 1 `Plus` Const 2)
simplArith :: Arith -> Maybe Arith
simplArith ex = case ex of
  Mult (Plus e1 e2) m ->
    Just $ Plus (Mult e1 m) (Mult e2 m)
  Mult m (Plus e1 e2) ->
    Just $ Plus (Mult m e1) (Mult m e2)
  Exp e 2 -> Just $ Mult e e
  _ -> Nothing

simplArith' :: Arith -> Arith
simplArith' e = fromMaybe e $ simplArith e

simplArithAll :: Arith -> Arith
simplArithAll ex = case ex of
  Mult e1 e2 -> let
    e1' = simplArith' e1
    e2' = simplArith' e2
    ex' = Mult e1' e2'
    in simplArith' ex'
  Plus e1 e2 -> let
    e1' = simplArith' e1
    e2' = simplArith' e2
    in Plus e1' e2'
  Exp inner n -> let
    inner' = simplArith' inner
    e' = Exp inner' n
    in simplArith' e'
  Const x -> Const x
